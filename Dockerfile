# -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - --
FROM caddy:2-builder-alpine AS builder

RUN xcaddy build \
  --with github.com/greenpau/caddy-auth-jwt \
  --with github.com/greenpau/caddy-auth-portal \
  --with github.com/greenpau/caddy-trace


# -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - --
FROM caddy:2-alpine

#ENV PYTHONUNBUFFERED=1
#RUN apk add --update --no-cache nss-tools python3
#RUN ln -sf python3 /usr/bin/python
#RUN python3 -m ensurepip
#RUN pip3 install --no-cache --upgrade pip setuptools
#RUN pip3 install --no-cache jinja2 pyyaml

CMD ["caddy", "run", "--config", "/etc/caddy/Caddyfile"]
#, "--adapter", "caddyfile"]
#CMD ["./gen_and_serve.sh"]

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

#ADD caasapgen/gen_and_serve.sh gen_and_serve.sh
#ADD caasapgen/Caddyfile.jinja /etc/caddy/Caddyfile.jinja
#ADD caasapgen/gen.py gen.py