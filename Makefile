# docker image tag
TAG=dev


setup:
	pyenv local 3.9.1
	poetry install


build-image:
	# docker login registry.gitlab.com
	docker build -t registry.gitlab.com/arundo-tech/caasap:$(TAG) .
	docker build -t registry.gitlab.com/arundo-tech/caasap/target:$(TAG) -f Dockerfile_target .


publish-image: build-image
	docker push registry.gitlab.com/arundo-tech/caasap:$(TAG)
	docker push registry.gitlab.com/arundo-tech/caasap/target:$(TAG)