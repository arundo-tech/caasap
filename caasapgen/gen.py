import yaml
import jinja2
import sys

print('caasap gen')

# arguments
rules_config_path = sys.argv[1] if len(sys.argv) > 1 else 'examples/rules.yaml'
template_path = sys.argv[2] if len(sys.argv) > 2 else 'caasapgen/Caddyfile.jinja'
output_path = sys.argv[3] if len(sys.argv) > 3 else 'caasapgen/Caddyfile'



jwt_rule_nb = 0
def jwt_bloc(rule):
    global jwt_rule_nb
    if not rule.get('public', False):
        jwt_rule_nb += 1
        if jwt_rule_nb == 1:
            return True
    if 'roles' in rule:
        return True
    return False

def jwt_bloc1():
    return jwt_rule_nb == 1

#rules_config = dict(name='John Doe')
rules_config = yaml.load(open(rules_config_path), Loader=yaml.FullLoader)
assert rules_config['token']['type'] == 'jwt'

template_def = open(template_path).read()

#template = jinja2.Template(template_def)
env = jinja2.Environment(loader=jinja2.BaseLoader)

template = env.from_string(template_def)
template.globals['jwt_bloc'] = jwt_bloc
template.globals['jwt_bloc1'] = jwt_bloc1

template_rendered = template.render(**rules_config)

open(output_path, 'w').write(template_rendered)

print(template_rendered)
