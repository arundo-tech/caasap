# caasap

caasap (**C**addy **A**s **A** **S**imple **A**uth **P**roxy)
is a simple authentication proxy.

It is meant to be run in front of a target web application.
It will bring to the target app the authentification and authorization features.

The target app could be deployed in many ways :
* a kubernetes service
* a docker container
* a local running application
* or any existing application

## Example protecting a k8s service

```bash
# deploy
kubectl apply -f examples/caasap-target.yaml
#TODO: wait for the pod to start
#TODO: get the service IP (10.1.1.251)
```

The unprotected target app is accessible at http://10.1.1.251:8000

Start the auth proxy :
```
docker run \
    -v $PWD/examples/Caddyfile_k8s_target:/etc/caddy/Caddyfile \
    -v $PWD/examples/users.json:/etc/caddy/users.json \
    -v $PWD/examples/rules.yaml:/etc/caddy/rules.yaml \
    -p 8080 \
    -p 8443 \
    --network="host" \
    registry.gitlab.com/arundo-tech/caasap
```

Visit https://localhost:8443


## Example protecting a docker container


We will use a docker image serving static web pages from this repo :

```
docker run \
    -p 8000 \
    registry.gitlab.com/arundo-tech/caasap-target
```

The unprotected target app is accessible at http://localhost:8000/

Start the auth proxy :
```
docker run \
    -v $PWD/examples/Caddyfile_localhost_target:/etc/caddy/Caddyfile \
    -v $PWD/examples/users.json:/etc/caddy/users.json \
    -p 8080 \
    -p 8443 \
    --network="host" \
    registry.gitlab.com/arundo-tech/caasap
```

Visit https://localhost:8443

## Example protecting a local running application

We will use local static web pages from this repo.
We will serve them with a simple python webserver :

```
cd public
python3 -m http.server
```

The unprotected target app is accessible at http://localhost:8000/

We start our auth proxy :
```
docker run \
    -v $PWD/examples/Caddyfile_localhost_target:/etc/caddy/Caddyfile \
    -v $PWD/examples/users.json:/etc/caddy/users.json \
    -p 8080 \
    -p 8443 \
    --network="host" \
    registry.gitlab.com/arundo-tech/caasap
```


http://localhost:8080/caasap

Visit https://localhost:8443


## Example protecting an existing application

We will use the published pages of this project as an existing target app.

The unprotected target app is accessible at http://arundo-tech.gitlab.io/caasap/


```
docker run \
    -v $PWD/examples/Caddyfile_existing_target:/etc/caddy/Caddyfile \
    -v $PWD/examples/users.json:/etc/caddy/users.json \
    -p 8080:8080 \
    -p 8443:8443 \
    registry.gitlab.com/arundo-tech/caasap
```


http://localhost:8080/caasap

https://localhost:8443/caasap

## Expected features

* [x] based on caddy2 
* [x] auth portal plugin
* [x] public docker image
* [ ] public helm chart
* [ ] sample test app
* [ ] https frontend
* [ ] http backend (no https)


## caddy

plugins included :

* [github.com/greenpau/caddy-auth-portal](https://github.com/greenpau/caddy-auth-portal)
* [github.com/greenpau/caddy-auth-jwt](https://github.com/greenpau/caddy-auth-jwt)
* [github.com/greenpau/caddy-trace](https://github.com/greenpau/caddy-trace)


## default users

We included those users in the example local users database :
* user : `webadmin`, password : `webadmin`
* user : `alice`, password : `alice`
* user : `bob`, password : `bob`

```shell
# install bcrypt tool
go get -u github.com/bitnami/bcrypt-cli
# to generate a password
echo -n "password123" | bcrypt-cli -c 10
```

## build

```shell
# build the docker image
make build-image
# publish the docker image on gitlab.com registry
make publish-image
```
## misc

You can check the JWT token at [jwt.io](https://jwt.io/).